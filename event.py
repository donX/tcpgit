# The timer module
# A note on behaviour and usage:
# - You can call the set timer multiple times.
# - WaitForEvent() returns a timeout event.
import select, time
from heapq import *
from utils.utils import *
from random import random
MAX_PKT_SIZE = 4096

DEBUG = False

class Event:
  def __init__(self, sock):
    self.sock = sock
    self.queue = []
    self.seq_time_entry = {}
    self.killedSeq = {}

  def setTimer(self, seq_no, delta=550.0):
    """ sets a timer which times out after delta milliseconds"""
    if (DEBUG):
        print 'setting the timer', seq_no
    # find out the current time in milliseconds
    alarm_time = time.time()*1000 + float(delta)
    heappush(self.queue, (alarm_time, seq_no))  # min-heap
    # the time of reception of this packet must be before this time.
    self.seq_time_entry[seq_no] = alarm_time  # This is a cheap hack

  # You can clear a timer, after it timed out
  #  - which effectively defeats the purpose
  # So I do not know why do I have this function?
  # How about just deleting the entry?
  def clearTimer(self, seq_no):
    if seq_no in self.seq_time_entry:
      del self.seq_time_entry[seq_no]

    # if self.killedSeq.contains(seq_no):
    #   del self.killedSeq[seq_no]
    #   self.killedSeq[seq_no] = self.killedSeq[seq_no] + 1.0

  def clearAll(self):
    self.queue = []
    self.seq_time_entry = {}

  def waitForEvent(self):
    # Time Sensitive Things happen here...
    if (DEBUG):
        print self.queue

    while True:
      if not self.queue:
        Error('Nothing to wait for')
        return ('N0', -1)

      nextTimer = self.queue[0]
      timeout = nextTimer[0] - time.time()*1000

    #   Error(str(nextTimer))
    #   Error(str(timeout))

      actual_alarm_time = nextTimer[0]
      seq_no = nextTimer[1]

      if seq_no not in self.seq_time_entry:
        heappop(self.queue)
        continue

      read = []
      if (timeout > 0):
        read, w, e = select.select([self.sock], [], [], timeout/1000.0)

      if not read:
        # if (actual_alarm_time <= self.seq_time_entry[seq_no]):
        heappop(self.queue)
        del self.seq_time_entry[seq_no]
        return ('T0', seq_no)
      else:
        data, _ = self.sock.recvfrom(MAX_PKT_SIZE)
        ack_no = int(getAckSeqNo(data))
        self.clearTimer(ack_no)
        if (DEBUG):
          print ('ack=', ack_no)
        return ('ACK', ack_no)

        while True:
            read, _, _ = select.select([self.sock], [], [], 0.0)
            if read:
                data, _ = self.sock.recvfrom(MAX_PKT_SIZE)
                new_ack_no = int(getAckSeqNo(data))
                self.clearTimer(new_ack_no)
                ack_no = max(new_ack_no, ack_no)
            else:
                return ('ACK', ack_no)

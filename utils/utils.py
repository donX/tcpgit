# utils.py - small utility functions...
from log import *
import json, base64
import matplotlib.pyplot as plt

def loadsNoEx(data):
	try:
		dct = json.loads(data)
	except:
		Error('Malformed packet. pkt=: %s'%data)
		return {}
	return dct

# --------convenience methods----------
def getField(data, item):
	dct = loadsNoEx(data)
	return dct.get(item)

def getAckSeqNo(data):
	"""The sequence no that the sender is expencting next"""
	return getField(data, 'ack_num')

def getRxSeqNo(data):
	"""The sequence no of the pkt received"""
	return getField(data, 'seq_num')

def getPayload(data):
	return base64.b64decode(getField(data, 'payload'))

def get_file_sz(filename):
	import os
	return os.stat(filename).st_size

def plot():
	X = []
	Y = []
	f = open('logs/window.txt')
	for lines in f:
	    t, w = [float(x) for x in lines.strip().split(' ')]
	    X.append(t)
	    Y.append(w)

	plt.plot(X,Y)
	plt.show()


# def convert(x):
#   return ''.join([str(ord(p)) for p in x])

# def back_convert(x):
#   return ''.join([chr(p) for p in x.split(' ')])

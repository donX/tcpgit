# tcp.py
# Currently supports only one client to be connected at a time...

import socket, sys, select, time
from utils.log import *
from utils.utils import *
from TcpHdr import *
from event import Event
from CongestionWindow import *
from random import random

RX_BUFF_SIZE = 20000  # CW must be STRICTLY less than half of this.
MAX_PKT_SIZE = 8096  # There is no way to be sure that this will work.
SAFETY_TIMEOUT = 5   # reception for a udp pkt times out in 5 seconds.
MAX_PAYLOAD_SIZE = MAX_PKT_SIZE/2   # 4 KB packets

HOST = '0.0.0.0'
DEBUG = False
LOSS_PROBABILITY = 0.0000000001
SERVER_SIDE_LOSS_PROBABILITY = 0.00001
# MSG_SZ = 5000000

if (DEBUG):
    MAX_PAYLOAD_SIZE = 4
    RX_BUFF_SIZE = 10
    LOSS_PROBABILITY = 0.5
    MSG_SZ = 10

class MyTcp:
    def __init__(self, msg):
        Info('Class MyTcp object created. msg = %s'%msg)
        self.is_connected = False
        self.toHost = None
        self.toPort = None
        self.rx_seq_no = None # seq_no of last received contigous pkt - only till hs
        self.tx_seq_no = None # this is reset while sending "syn+ack"
        self.header = TcpHdr()
        self.RxBuff = None
        self.payloadRx = 0

    # sends the file...
    def send(self):
        message = self.message
        no_chunks = len(message)/MAX_PAYLOAD_SIZE + int((len(message)%MAX_PAYLOAD_SIZE) > 0)
        real_tcp_bytes_tx = [0 for i in range(no_chunks+1)]

        def _get_seq_no(chunk_no):
            return chunk_no + 1

        start_seq_no = 1
        def _get_chunk_data(chunk_no):
            msg = message[
                MAX_PAYLOAD_SIZE*chunk_no : (chunk_no+1)*MAX_PAYLOAD_SIZE
            ]
            return msg

        Cwnd = CongestionWindow()
        Rx_handler = Event(self.sock)

        last_acked_chunk_no = -1
        unacked_chunks = 0

        def _send_next_pkt(chunk_to_send=None):
            if (chunk_to_send == None):
                chunk_to_send = last_acked_chunk_no + unacked_chunks + 1

            if (chunk_to_send > no_chunks):
                return False

            s_no = _get_seq_no(chunk_to_send)
            if (random() < SERVER_SIDE_LOSS_PROBABILITY):
                Rx_handler.setTimer(s_no)
                Error('Sim pkt loss for %d'%s_no)
                return True

            packet = TcpHdr.formPkt(
                s_no,
                _get_chunk_data(chunk_to_send)
            )

            real_tcp_bytes_tx[chunk_to_send] = len(packet) - 30

            self.__sendUDP(
                packet,
                self.toHost,
                self.toPort,
            )
            Rx_handler.setTimer(s_no)
            return True

        pr_okblue('sending begins: %.2f'%time.time())
        just_before_sending = time.time()
        _send_next_pkt()
        unacked_chunks += 1
        last_ack = -1
        ack_count = 0
        while True:
            (evt, data) = Rx_handler.waitForEvent()
            if (evt == 'T0'):
                s_no = data     # basically data means s_no here.
                Error('Timeout for ' + str (s_no) )
                Rx_handler.clearAll()
                unacked_chunks = 0
                _send_next_pkt()
                Cwnd.timeout()
                unacked_chunks = 1

            elif (evt == 'ACK'):
                chunk_ack_no = data - 1
                if (chunk_ack_no < last_acked_chunk_no):
                    continue

                if chunk_ack_no == last_ack:
                    ack_count += 1
                else:
                    last_ack = chunk_ack_no
                    ack_count = 1
                triple_dup_detected = (ack_count == 3)
                
                # first serve the ack.
                if (chunk_ack_no == no_chunks):
                    print chunk_ack_no, no_chunks
                    end_time = time.time()
                    time_elapsed = end_time-just_before_sending
                    Info('\n----------------------Transfer Complete---------------------')
                    pr_okblue('The client must have received the file')
                    pr_okblue('timestamp: %.2f s, total=%.2f s, Throughput=%.2f Mbps'%
                        (end_time, 
                        time_elapsed, 
                        (8.0*sum(real_tcp_bytes_tx)/(1024*1024))/time_elapsed),
                    )
                    return True

                if not triple_dup_detected:
                    Cwnd.incr_cw()
                    # TODO: Maybe we need to clear some timers? perhaps NO
                    unacked_chunks = (last_acked_chunk_no + unacked_chunks) - chunk_ack_no
                    last_acked_chunk_no = chunk_ack_no
                else:
                    Warn('%d Triple Duplicate Detected for %d'%(time.time(), last_ack))
                    Rx_handler.clearAll()
                    unacked_chunks = 0
                    Cwnd.dupAck()
                    Cwnd.incr_cw()  # okay, ek badha bhi de :p
            
            for i in range(unacked_chunks, Cwnd.get_cw()):
                _send_next_pkt()
                unacked_chunks += 1


    # This is a receive file function actually.
    def recv(self, max_bytes=100000000, f_name='received_file.txt'):
        bytes_rx = 0
        real_tcp_bytes_rx = 0
        rx_buff = [None]*RX_BUFF_SIZE
        start_index = 0

        # The min tcp seq_no which we are expecting first
        #   = min seq no not recvd
        next_tcp_seq_no = 1
        f = open(f_name, 'w')

        # INTERNAL FN: perhaps its not a good practise
        # assuming we are not taking care of bad nos - we are actually
        def _seq_no_to_buff_ind(seq_no):
            return (seq_no - next_tcp_seq_no + start_index)%RX_BUFF_SIZE

        def _ack():
            self.__acknowledge(next_tcp_seq_no-1)
            return next_tcp_seq_no-1

        st = time.time()
        packet_count=0
        noise = 0
        acked_at = 0
        last_acked = 0

        while True:
            data = self.__recvUDP(0.12)
            if not data:
                noise += 1
                continue

            noise += 1
            seq_no = int(getRxSeqNo(data))
            packet_count += 1

            if DEBUG or (packet_count%1000 == 0):
                pr_okblue( 'rx seq=%d, start=%d, val=%d, time=%f'
                    %(next_tcp_seq_no-1, start_index, next_tcp_seq_no, 1000*(time.time()-st))
                )

            if (seq_no < next_tcp_seq_no):
                # Error('Duplicate pkt rcvd %d'%seq_no)
                continue

            real_tcp_bytes_rx += len(data) - 30
            buff_index = _seq_no_to_buff_ind(seq_no)
            rx_buff[buff_index] = getPayload(data)

            while rx_buff[start_index] != None:
                noise += 1
                bytes_rx += len(rx_buff[start_index])
                f.write(rx_buff[start_index])
                if (rx_buff[start_index] == '' or bytes_rx >= max_bytes):
                    self.__acknowledge(next_tcp_seq_no)
                    f.close()
                    return real_tcp_bytes_rx

                rx_buff[start_index] = None
                start_index = (start_index + 1)%RX_BUFF_SIZE
                next_tcp_seq_no += 1

                if (next_tcp_seq_no-1 < 200) :
                    last_acked = _ack()
                    acked_at = noise
                else:
                    if (noise - acked_at > 3) :
                        last_acked = _ack()
                        acked_at = noise
                    else:
                        noise += 1

            if noise - acked_at > 3:
                last_acked = _ack()
                acked_at = noise

    # For client(s)
    def connect(self, host, port):
        self.toHost = host; self.toPort = port
        pr_okblue ('connecting to %s:%d'% (host, port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 1024*1024*3)
        status = self.__handShake()
        if not status:
            Error('Sorry man, could not establish connectection to the server in 1 attempt. (lazy client)')
            return False
        Info('connection established (Tcp handshake completed)')
        # set some connection params

    # For server
    def beginServer(self, port, filename):
        self.port = port
        self.host = HOST     # change to '0.0.0.0' for wifi and stuff
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 1024*1024*2)
        # Bind the socket to the port
        server_address = (self.host, port)
        self.sock.bind(server_address)
        # Now we will have an infinite loop here...
        Warn('Loading file.. Please wait')
        self.message = bytes(open(filename).read())    # takes a while
        Info('file server ready to serve %s on %s:%s'%((filename,) + server_address))

        while True:
            handShakeResult = self.__waitAndHandShake()
            if (handShakeResult == False):
                continue
            pr_okblue('Connection established with client')
            self.send()
            plot()


    def __acknowledge(self, seq_no):
        """tell the other side that I have received this seq_no"""
        if DEBUG:
            print 'acking: ', seq_no
        ackPkt = self.header.formAckPkt(seq_no)
        self.__sendUDP(ackPkt, self.toHost, self.toPort)


    def __sendUDP(self, msg, host, port):
        """please make sure that msg is <= MAX_PKT_SIZE or it shall be trunc."""
        self.sock.sendto(msg, (host, port))


    def __recvUDP(self, timeout=SAFETY_TIMEOUT):
        """returns None in case of timeout"""
        (data, _) = self.__recvUDPWithAddr(timeout)

        if (random() < LOSS_PROBABILITY):
            if ('reset' not in loadsNoEx(data)):
                # Error('simulated pkt loss ' + str(data))
                Error('simulated pkt loss')
                return self.__recvUDP()

        if data:
            self.payloadRx += len(data)

        # if (DEBUG):
        #     if not data:
        #         Error('Dataless pkt rx')
        #     Info('Rx: %s'%data)
        #     pr_okblue ('total_bytes_rx=%d'%self.payloadRx)
        return data

    def __recvUDPWithAddr(self, timeout=SAFETY_TIMEOUT):
        # if (DEBUG):
        #     Info('waiting Rx')
        read, w, e = select.select([self.sock], [], [], timeout)
        if not read:
          # Error('Rx Timed out')
          return (None, None)
        data, address = self.sock.recvfrom(MAX_PKT_SIZE)

        # pr_okblue( 'got "%s" from %s'%(data, repr(address)))
        return (data, address)

    def __isSynOK(self, data):
        dct = loadsNoEx(data)
        print dct
        if dct.get('syn') == True and dct.get('ack') == False:
          return True
        return False

    def __isAckOK(self, data):
        dct = loadsNoEx(data)
        if dct.get('syn') == False and dct.get('ack') == True:
            return True
        return False

    def __isSynPlusAckOK(self, data):
        dct = loadsNoEx(data)
        if dct.get('syn') == True and dct.get('ack') == True:
            return True
        return False

    def __handShake(self):
        synPkt = self.header.getSynPkt()
        self.tx_seq_no = self.header.get_seq_num()

        # send syn
        self.__sendUDP(synPkt, self.toHost, self.toPort)
        # receive synAck
        synAck = self.__recvUDP()
        if not self.__isSynPlusAckOK(synAck):
            return False

        self.rx_seq_no = getRxSeqNo(synAck)
        Error(str(self.rx_seq_no))

        # send ack
        ackPkt = self.header.getAckPkt(self.rx_seq_no)
        self.__sendUDP(ackPkt, self.toHost, self.toPort)
        return True

    # server side (right side)
    # returns True or False depending on stuff...
    def __waitAndHandShake(self):
        self.header = TcpHdr()

        (syn, addr) = self.__recvUDPWithAddr()
        if not self.__isSynOK(syn):
            Error('Client send bad syn packet/malformed pkt')
            return False

        self.rx_seq_no = getRxSeqNo(syn)
        host = addr[0]; port = addr[1]
        self.tx_seq_no = self.header.get_seq_num()
        synAckPkt = self.header.getSynAckPkt(self.rx_seq_no)
        self.__sendUDP(synAckPkt, host, port)    # TODO: header

        ack = self.__recvUDP()
        if not self.__isAckOK(ack):
            return False

        rx_seq_no_in_ack = getAckSeqNo(ack)
        if rx_seq_no_in_ack != (self.tx_seq_no + 1):
            Error('client sent bad ack no. expected: %d, rec: %d' \
                %((self.tx_seq_no + 1), rx_seq_no_in_ack))
            return False

        self.toHost = host; self.toPort = port
        return True

    def close(self):
        if (self.sock):
            self.sock.close()

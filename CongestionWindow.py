# deal with non integral CW values

import sys
from log import *
import time

WINDOW_LOG_FILE = 'logs/window.txt'

class CongestionWindow:
    def __init__(self):
        self.cw = 1.0
        self.ssthresh = sys.maxint
        self.f = open(WINDOW_LOG_FILE, 'w')
        self.st = time.time()

    def log_wind(self):
        line = str(time.time()-self.st) + ' ' + str(self.cw) + '\n'
        self.f.write(line)

    def get_cw(self):
        self.log_wind()        
        return int(self.cw)

    def slowstart(self):
        self.cw += 1.0
        self.log_wind()

    def add_incr(self):
        self.cw += 1.0/self.cw
        self.log_wind()

    def timeout(self):
        self.ssthresh = self.cw/2
        self.cw = 1.0
        self.log_wind()
        # self.cw = self.cw/2

    def dupAck(self):
        self.ssthresh = self.cw/2
        self.cw = self.cw/2
        self.log_wind()

    def incr_cw(self):
        if self.cw >= self.ssthresh:
            self.add_incr()
        else:
            self.slowstart()
        self.cw = min(500.0, self.cw)
        self.log_wind()

# test
if __name__ == '__main__':
    con = CongestionWindow()
    con.incr_cw()
    con.incr_cw()
    con.incr_cw()
    print con.ssthresh
    print con.get_cw()
    con.dupAck()
    con.incr_cw()
    print con.ssthresh
    print con.get_cw()

# server.py
# putpose: wait for a client to connect ans send a file to him ONLY
import sys
from tcp import MyTcp
from utils.log import *

pr_okmegenta('Example usage: python server.py 5001 files/to_send.txt')

conn = MyTcp('server - bazooka')
port = 5000
filename = 'files/to_send.txt'

if len(sys.argv) > 1:
    port = int(sys.argv[1])

if len(sys.argv) > 2:
    filename = sys.argv[2]

conn.beginServer(port, filename)

# try:
#     conn.beginServer(5000)
# except:
#     # close by ctrl-c
#     Error('You killed ME! \n\t- dead server :p')
#     print(sys.exc_info()[0])
#     conn.close()



# creating files
# size = 50000000
# f = open('to_send.txt', 'w')
# f.write(''.join(['ab' for i in range(size/2)]))
# f.close()
import matplotlib.pyplot as plt

X = []
Y = []
f = open('logs/window.txt')

for lines in f:
    t, w = [float(x) for x in lines.strip().split(' ')]
    X.append(t)
    Y.append(w)

plt.plot(X,Y)
plt.show()
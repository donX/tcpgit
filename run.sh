#!bin/bash

echo 'trying to kill all previous servers'
screen -X -S 'server' quit

echo 'starting server'
screen -dmS 'server' python server.py
echo 'starting client'
python client.py

import random, math, json
from utils.log import *
import base64

class TcpHdr:
	def __init__(self):
		self.src_port = None
		self.dst_port = None
		# self.seq_num = random.randint(0, math.pow(2,32)-1)
		self.seq_num = 0    # Lets start with 0!
		self.ack_num = None
		self.hdr_len = None

		# flags
		self.syn = False
		self.fin = False
		self.reset = False
		self.push = False
		self.urg = False
		self.ack = False

		self.adv_window = None
		self.checksum = None
		self.urg_ptr = None

	@staticmethod
	def formPkt(seq_no, payload):
		"""Returns the full packet to be sent over UDP"""
		dct = {
			'seq_num': seq_no,			# IMP.
			'ack': False,
			'payload': base64.b64encode(payload),
		}

		UDP_payload = json.dumps(dct)
		# Info('extra payload = %d'%(len(UDP_payload)-20-len(payload)))
		return UDP_payload

	@staticmethod
	def formAckPkt(seq_no):
		"""Returns the full packet to be sent over UDP"""
		dct = {
			'ack_num': seq_no,
			'ack': True,
			'payload': '',
		}

		UDP_payload = json.dumps(dct)
		# Info('extra payload = %d'%(len(UDP_payload)-20-len(payload)))
		return UDP_payload


	# this is not optimized, BUT optimizing it (byte alignment etc) is not the
	#  essential part of the project & we are not doing dirty coding
	def getPkt(self, payload):
		"""
			returns a packet, with increasing sequence no.
		"""
		# computedCheckSum =
		dct = {
			'src_port': self.src_port,	# not req yet
			'dst_port': self.dst_port,	# not req yet
			'seq_num': self.seq_num,			# IMP.
			'ack_num': self.ack_num,			# IMP.
			# 'hdr_len': 20, # who cares
			'syn': self.syn,
			# 'fin': self.fin,
			'reset': self.reset, # we don't care
			# 'push': self.push,
			# 'urg': self.urg,
			'ack': self.ack,
			# 'adv_window': self.adv_window,
			# 'checksum': self.checksum,  # we shall compute some sort of checksum
			# 'urg_ptr': self.urg_ptr,
			'payload': payload
		}

		self.seq_num += 1
		UDP_payload = json.dumps(dct)
		# Info('extra payload = %d'%(len(UDP_payload)-20-len(payload)))
		return UDP_payload

	# convenience methods
	def getSynPkt(self):
		# It resets flags and seq_number
		self.resetFlags()
		self.syn = True
		# self.seq_num = random.randint(0, math.pow(2,32)-1)
		self.seq_num = 0
		return self.getPkt('')	# 0 byte payload

	def getSynAckPkt(self, seq_no_RX):
		self.resetFlags()
		# self.seq_num = random.randint(0, math.pow(2,32)-1)
		self.seq_num = 0
		self.ack = True
		self.ack_num = seq_no_RX+1
		self.syn = True
		return self.getPkt('')

	# seq_no is the last seq Rx
	def getAckPkt(self, seq_no):
		self.resetFlags()
		self.ack = True
		self.ack_num = seq_no+1
		return self.getPkt('')


	def resetFlags(self):
		self.syn = False
		self.fin = False
		self.reset = False
		self.push = False
		self.urg = False
		self.ack = False

	# ----------------------- not really useful -------------
	def set_src_port(self, x):
		self.src_port = x

	def get_src_port(self):
		return self.src_port

	def set_dst_port(self, x):
		self.dst_port = x

	def get_dst_port(self):
		return self.dst_port

	def get_seq_num(self):
		"""Returns the pkt no which shall be sent next"""
		return self.seq_num

	def set_ack_num(self, x):
		self.ack_num = x

	def get_ack_num(self):
		return self.ack_num

	def set_hdr_len(self, x):
		self.hdr_len = x

	def get_hdr_len(self):
		return self.hdr_len

	def set_syn(self, x):
		self.syn = x

	def get_syn(self):
		return self.syn

	def set_fin(self, x):
		self.fin = x

	def get_fin(self):
		return self.fin

	def set_reset(self, x):
		self.reset = x

	def get_reset(self):
		return self.reset

	def set_push(self, x):
		self.push = x

	def get_push(self):
		return self.push

	def set_urg(self, x):
		self.urg = x

	def get_urg(self):
		return self.urg

	def set_ack(self, x):
		self.ack = x

	def get_ack(self):
		return self.ack

	def set_adv_window(self, x):
		self.adv_window = x

	def get_adv_window(self):
		return self.adv_window

	def set_checksum(self, x):
		self.checksum = x

	def get_checksum(self):
		return self.checksum

	def set_urg_ptr(self, x):
		self.urg_ptr = x

	def get_urg_ptr(self):
		return self.urg_ptr



if __name__ == '__main__':
	a = TcpHdr()
	Info(a.getPkt('Hey there. This is a payload'))

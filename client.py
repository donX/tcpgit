# client.py
# purpose: connect to a server and receive a file (on TCP)

import sys, socket
from tcp import MyTcp
from log import *
import time

pr_okmegenta('python client.py 5001 192.168.1.104 files/reggae.mp4')
conn = MyTcp('client')
port = 5000
host = 'localhost'
rx_file = 'files/rx.txt'
server_file = 'files/to_send.txt'

if len(sys.argv) > 1:
    port = int(sys.argv[1])

if len(sys.argv) > 2:
    host = sys.argv[2]

if len(sys.argv) > 3:
    rx_file = sys.argv[3]

conn.connect(host, port)
a = time.time()
bytes_rx = conn.recv(f_name=rx_file)

delta_t = (time.time() - a)*1000
# Info('File contents: ' + repr(data))
Info('\n----------------------Transfer Complete---------------------')
pr_okblue( 'Time=%.2f s for %d MB Throughput = %.1f Mbps'%
    (delta_t/1000.0, bytes_rx/1024.0/1024.0, (8*bytes_rx/(1024.0*1024.0))/(delta_t/1000) )
)

# Info('Verifying data...')
# import filecmp
# result = filecmp.cmp(rx_file, server_file)
# if (result):
# 	pr_okblue('Success, the files %s, %s have exactly the same contents'%(rx_file, server_file))
# else:
# 	Error('burrpp! TCP-Katwaria failed!')	

# TODO: keep calling recv. till the file is received...
